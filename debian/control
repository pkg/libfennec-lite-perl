Source: libfennec-lite-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper (>= 8),
               libmodule-build-perl,
               perl
Standards-Version: 3.9.6
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-perl/packages/libfennec-lite-perl.git
Vcs-Git: git://anonscm.debian.org/pkg-perl/packages/libfennec-lite-perl.git
Homepage: https://metacpan.org/release/Fennec-Lite

Package: libfennec-lite-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends}
Description: minimalist Fennec implementation
 Fennec, a Perl test helper providing RSPEC, Workflows, Parallelization, and
 Encapsulation, does a ton, but it may be hard to adopt it all at once. It
 also is a large project, and has not yet been fully split into component
 projects. Fennec::Lite takes a minimalist approach to do for Fennec what
 Mouse does for Moose.
 .
 Fennec::Lite is a single module file with no non-core dependencies. It does
 not cover any of the more advanced features such as result capturing or SPEC
 workflows. This module only covers test grouping and group randomization. You
 can also use the FENNEC_ITEM variable with a group name or line number to run
 a specific test group only. Test::Builder is used under the hood for TAP
 output.
